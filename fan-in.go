package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"sync/atomic"
	"syscall"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage: fan-in COUNT")
		os.Exit(1)
	}

	if err := _main(os.Args[1]); err != nil {
		log.Fatal(err)
	}
}

func _main(nString string) error {
	nWriters, err := strconv.Atoi(nString)
	if err != nil {
		return err
	}
	if nWriters <= 0 {
		return fmt.Errorf("number of writers not positive: %d", nWriters)
	}

	var sleep time.Duration
	if x, err := strconv.Atoi(os.Getenv("SLEEP_US")); x > 0 && err == nil {
		sleep = time.Duration(x) * time.Microsecond
	}

	var nBytes int64
	errC := make(chan error)
	for i := 0; i < nWriters; i++ {
		go func() { errC <- handleWriter(&nBytes, os.Getenv("SOCKETPAIR") == "1", sleep) }()
	}

	select {
	case err := <-errC:
		return fmt.Errorf("writer subprocess: %w", err)
	case <-time.After(35 * time.Second):
		// Sleep long enough to capture a 30s CPU profile
	}

	log.Printf("bytes received: %gMB", float32(nBytes)/float32(1024*1024))

	return nil
}

func handleWriter(nBytes *int64, socketPair bool, sleep time.Duration) error {
	var stdoutW, stdoutR *os.File
	var err error
	if socketPair {
		fds, err := syscall.Socketpair(syscall.AF_LOCAL, syscall.SOCK_STREAM, 0)
		if err != nil {
			return err
		}
		stdoutR = os.NewFile(uintptr(fds[0]), "(socketpair)")
		stdoutW = os.NewFile(uintptr(fds[1]), "(socketpair)")
	} else {
		stdoutR, stdoutW, err = os.Pipe()
		if err != nil {
			return err
		}
	}
	defer stdoutR.Close()
	defer stdoutW.Close()

	cmd := exec.Command("./_writer/writer")
	cmd.Stdout = stdoutW
	if err := cmd.Start(); err != nil {
		return err
	}
	defer cmd.Wait()
	if err := stdoutW.Close(); err != nil {
		return err
	}

	buf := make([]byte, 32768)
	for {
		n, err := stdoutR.Read(buf)
		atomic.AddInt64(nBytes, int64(n))
		if err != nil {
			return err
		}
		if sleep > 0 {
			time.Sleep(sleep)
		}
	}
}
