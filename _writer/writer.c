#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

// Git upload-pack writes 5-byte headers followed by 8192-byte packets.
const char header[5], pkt[8192];

int main(void){
	char *bufsiz_str = getenv("WRITER_BUFFER");
	int bufsiz = bufsiz_str ? atoi(bufsiz_str) : 0;

	if (bufsiz > 0) {
		setvbuf(stdout, malloc(bufsiz), _IOFBF, bufsiz);

		for(;;) {
			fwrite(header, 1, sizeof(header), stdout);
			fwrite(pkt, 1, sizeof(pkt), stdout);
		}
	} else {
		for(;;) {
			write(1, header,sizeof(header));
			write(1, pkt,sizeof(pkt));
		}
	}
}