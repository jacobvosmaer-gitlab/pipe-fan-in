all: writer fan-in

export CFLAGS = -fno-omit-frame-pointer

writer:
	make -C _writer writer

fan-in: fan-in.go
	go build

clean:
	rm -f fan-in _writer/writer